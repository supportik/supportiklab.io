---
title: Contribute
subtitle: We need you!
date: 2018-03-03
comments: false
---


[GitLab Project - Supportik](https://gitlab.com/supportik/supportik)

---

## How to Contribute

1. [Setup Development Environment](#development-environment-setup)
1. [Log/Find a feature that requires action](https://gitlab.com/supportik/supportik/issues)
1. [Create a feature branch, commit and push changes, and a Open Pull Request](#your-first-contribution)

## Development Environment Setup

### Overview

*TBA*

See: [Development Details](#development-details)

### Step 1. In GitLab
1. Fork Supportik Repository
    1. [Login/Register](https://gitlab.com/users/sign_in) to GitLab - if you aren't already ;).
    1. Go to [GitLab Project: Supportik](https://gitlab.com/supportik/supportik)
    1. Click the Fork icon.

### Step 2. On Host Development Machine

1. Install Git
1. Install Docker - *[Appendix: Docker](#docker)*
1. Setup `devel` directory  
    *This is our root development directory which is shared with the development VM and containers.*
    - Windows: `C:/devel/`
    - Mac/Linux: `/devel/`
1. Clone your Fork locally
    1. Switch to `devel` directory
    1. `git clone https://gitlab.com/<username>/supportik.git`
1. Install an IDE (that supports PHP/Web)
    - Recommended: [VS Code](#vs-code-setup)
    - Alternatives:
        - *TBA*
1. Setup project in IDE
1. Run Docker ` - TBA - `
    - Adjust the local directory path to ensure local project is available in Docker container
    - If on Windows (especially if you don't have Hyper-V), use [Docker-Machine](#docker-machine)
1. Go to http://supportik.dev
1. Get modifying!

## Your first contribution

*TBA*

---

## Appendix

### Development Details
- Service Side Language: PHP 7
  - Framework: Silex
  - Testing Framework: *TBA - PHPUnit?*
  - Library Management: Composer
- Client Side Language: JS
  - Framework: VueJS
  - Static Type Checker: Flow
  - Testing Framework: *TBA*
- Database:
  - PostgreSQL 10.2
- Environment Management Tool:
  - Docker
- Source Control:
  - Git via GitLab
- Feature/Bug Tracking:
  - GitLab Issues

### VS Code Setup
*Incomplete* 

### Docker
You need to install Docker localy to run, test and develop. The simplest way to use Docker is with [Docker CE](https://docs.docker.com/install/). However, Docker CE does not run on all systems. You should check if your system is supported under System Requirements before downloading. If your system does not support Docker CE, you will need to use [Docker Toolbox](https://docs.docker.com/toolbox/overview/). Either will work for the purpose of development of Supportik.
