---
title: Arrow
subtitle: A ThePHPLeague Framework
comments: false
---

\* __Currently under development / Watch this space.__ *

## About
Introducing [Arrow](/arrow)! The no fuss framework built on the components of a highly respected collection of moderated components held to a high standard of 

The name and it's purpose comes from the php arrow "->".

`$yourApplication->doIt()`.

Currently it is still in very early stages, and doesn't even have a release version yet.

### Details
It has been developed with the [Event](http://event.thephpleague.com/2.0/), [Router](http://route.thephpleague.com/) and [Container](http://container.thephpleague.com/) components from [The PHP League](http://thephpleague.com/) at it's heart. It also comes with Middleware support, allowing you to hook into the system to add your own required functionality where required.

It is setup as a composer package, but is not available from [packagist](https://packagist.org/) yet, although can be loaded by adding as a custom composer repository (see Installation below).

## Installation
Install using Composer, but only as a private repository at the moment.

Update `composer.json` -
```json
{
  "require": {
    "php": "^7.2",
    "arrowphp/arrow": "@dev"
  },
  "repositories": [
    {
      "type": "vcs",
      "url": "https://gitlab.com/arrowphp/arrow"
    }
  ],
  ...
}

```

## Usage
Example `index.php` (using Guzzle) -
```php
<?php

require_once __DIR__.'/../vendor/autoload.php';

$app = new \Arrow\Application(
	new \Arrow\Config('/devel/supportik/')
);

$request = \GuzzleHttp\Psr7\ServerRequest::fromGlobals();
$response = new \GuzzleHttp\Psr7\Response();

$response = $app->run($request, $response);
$app->flush($response);
$app->terminate();
```