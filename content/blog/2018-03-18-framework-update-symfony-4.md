---
title: Framework Update - Symfony 4
date: 2018-03-18
comments: false
---

Update 2018-04-04: I decided not to go with Symfony 4. See my update [here](/blog/2018-04-04-framework-update-arrow).

---

Quick update to this point: Docker is now working and serving a requests to the browser. There is still more to do before this is the ideal setup, but it works.

---

### Silex

So, originally the plan was to use the [Silex](https://silex.symfony.com/) framework for this project. It is built on the Symfony framework and has plenty of documentation and support. All was good until I discovered [Symfony is no longer going to support Silex](https://symfony.com/blog/the-end-of-silex). Which is disappointing because I love Symfony, but more importantly, I loved that I didn't have to write the frameowrk.

So, enter [Symfony 4](https://symfony.com/4). First impressions are good. It's slimmer, lighter, presumably faster, and has the same great documentation. However, given the changes made to the components and the updated application flow, existing Symfony 3 based frameworks would not be ported to Symfony 4 (any time soon). So I decided to start my own.

### Sympera

Introducing [Sympera - A Symfony 4 Framework](/sympera), originally developed right here for Supportik. Right now it is committed directly into the Supportik project, but eventually will be spawned off into it's own Composer Package. 

Keep an eye on these blogs to see the progress of Sympera as well as Supportik.
