---
title: It has started!
date: 2018-03-03
comments: false
---

Hello you! My name is [Chris Pennycuick](https://gitlab.com/christopher.pennycuick), and I am the original author of this project.

I have been thinking about doing an open-source help-desk SaaS application for a while with a strong focus on enabling external integrations. So I decided to just sit down and push out as much as I can in my downtime to get the ball rolling.

The name "Supportik" is an amalgamation of "Support" and "Ticket". Not that original, but I think it's kinda catchy. It may even develop into a verb "Supportik it" - but I doubt it... oh well.

Right now everything is still in planning phase and I am still deciding on some things which cover all aspects of the project. I aim to use the most supported new libraries and patterns. I am working right now to document the environment setup for developers to allow/encourage contributions from as early as possible. I'll post another update once I have locked a few more things down.

---

PS. The dream is that this idea/hobby/project turns into something more ... but isn't that every developers dream - for their hobby project to turn into their own profitable business. Well, maybe one day... and if you are a significant contributor, I may just invite you to come with me.
