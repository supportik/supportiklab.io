---
title: Framework Update - Arrow
date: 2018-04-04
comments: false
---

TLDR; I abandoned Sympera and Symfony 4 because of the complexity of their container. I chose to combine components from [The PHP League](http://thephpleague.com/) instead. So with a few enhancements and an Application class wrapper, a framework was born.

---

Okay, so I have ditched Sympera. The real problem was Symphony's Container, it didn't behave in a way I needed it to. Well, at least not that I could find looking through their documentation or by looking through the source. And after a little looking around, I stumbled upon [The PHP League](http://thephpleague.com/) and their Container.

### Arrow

Introducing [Arrow](/arrow)! It has been developed with the [Router](http://route.thephpleague.com/) and [Container](http://container.thephpleague.com/) from [The PHP League](http://thephpleague.com/) at it's heart. Currently still in very early stages it is setup as a composer package, but is not available from [packagist](https://packagist.org/) yet, but can be loaded by adding a custom composer repository. To find out more information jump over to [Arrow's page](/arrow).

### Progress

With Arrow up and running, we have now made some progress on beginning the application. It is responding to routes, loading modules, and serving content. There are still a few design decisions which I would like to work through first before really starting development, such as how Client/Server side will be split yet work together.

### Next Steps

Hopefully with the next update, there will be a simple UI, API, and DB integration, the beginning of the app. Additionally: I am considering using a non-relational DB to learn that technology.