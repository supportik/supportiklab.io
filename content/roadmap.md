---
title: Roadmap
subtitle: Where do you see yourself in ...
comments: false
---

If you think something is missing from here, please raise a support issue.

## Available Features:

- *Nothing yet* :cry: 

## Features In Development:

- *Nothing yet* :cry:

## Planned Features - in no order:

- Users
- Companies
- Auditing
- FAQ
- Tags
- Tickets
- Articles
- API
- Web-Hooks
- Web-Sockets
